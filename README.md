# VAD + Whisper AI Transcriber

### Pre-trained Models

- [Silero VAD](https://github.com/snakers4/silero-vad) (Voice Activity Detector)
- [PyAnnote](https://huggingface.co/pyannote)
- [Whisper](https://github.com/openai/whisper)

### How it works

1.  Converts audio into `.wav` (the format the models were trained on).
2.  Performs VAD using `silero-vad` and dumps the slice info into json.
3.  Uses pyannote.audio to diariatize the audio and annotate the json slice info from the previous step.
4.  Slices the audio into real audio chunk files.
5.  Feeds each audio chunk file into whisper.
6.  Combines audo chunk transcripts into a full transcript file.

_A .json file will be created for each step for troubleshooting and restarting from any one step_

## Getting started

Create account on [Hugging Face](https://huggingface.co) and generate READ token in order to download [PyAnnote](https://huggingface.co/pyannote) models.

Place token in `.env` file as such:

```
PYANNOTE_KEY=*****************************
```

For installation please install the necessary dependencies in `requirements.txt` and also refer to [PyTorch](https://pytorch.org/get-started/previous-versions/) for cuda-specific support. To find your cuda version, you may run the following commands in your terminal:

- `nvidia-smi` for Windows
- `nvcc --version` for Linux

Edit the `requirements.txt` file with the necessary and available torch, torchaudio, and torchvision packages.

#### Example (Cuda 11.7):

```
torch==1.13.1+cu117
torchaudio==0.13.1+cu117
torchvision==0.14.1+cu117
```

### Installation

```
pip install -r requirements.txt --find-links https://download.pytorch.org/whl/torch_stable.html
```

### Running GUI

```
python VADtranscriber.py
```

_Tested and created with Python 3.8_
